﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Globalization;
using System.IO;

namespace SkyriseProject
{
    class CsvMapper
    {
        public Record FromStreamToRecord(Stream stream)
        {
            var record = new Record();

            using (TextFieldParser parser = new TextFieldParser(stream))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                if (!parser.EndOfData)
                {
                    // Skip header
                    parser.ReadLine();

                    string[] fields = parser.ReadFields();

                    var culture = new CultureInfo("en-US");

                    var dateTime = $"{fields[1]} {fields[2]}";

                    record.Symbol = fields[0];
                    record.DataCzas = DateTime.Parse(dateTime);
                    record.Otwarcie = Convert.ToDecimal(fields[3], culture);
                    record.Najwyzszy = Convert.ToDecimal(fields[4], culture);
                    record.Najnizszy = Convert.ToDecimal(fields[5], culture);
                    record.Zamkniecie = Convert.ToDecimal(fields[6], culture);
                    record.Wolumen = Convert.ToDecimal(fields[7], culture);
                }
            }
            return record;
        }
    }
}
