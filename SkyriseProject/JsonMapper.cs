﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SkyriseProject
{
    class JsonMapper
    {
        public List<Record> FromJsonToRecords(string readContent)
        {
            return JsonConvert.DeserializeObject<List<Record>>(readContent);
        }

        public string FromRecordsToJson(List<Record> records)
        {
            return JsonConvert.SerializeObject(records, Formatting.Indented);
        }
    }
}
