﻿using System;
using System.Threading;

namespace SkyriseProject
{
    class ProgramLoop
    {
        public static string ChosenFilePath;
        public static int RefreshIntervalInSeconds;

        public void RunWithTimeInterval()
        {
            Console.WriteLine("Running main functionality...");

            while(true)
            {
                Run();
                Console.WriteLine($"Waiting for {RefreshIntervalInSeconds} seconds...");
                Thread.Sleep(RefreshIntervalInSeconds * 1000);
            }
        }

        private void Run()
        {
            var filesRepo = new FilesRepo();
            WebService webService = new WebService();

            var recordsFromFile = filesRepo.LoadLatestJsonFile(ChosenFilePath);
            var recordsFromWeb = webService.GetDataFromWeb();

            if(recordsFromFile == null)
            {
                Console.WriteLine("Saving 1st file...");
                filesRepo.SaveQuotations(recordsFromWeb);
                Console.WriteLine("1st file saved.");
            }
            else
            {
                if (webService.IsRecordUpdated(recordsFromWeb, recordsFromFile) == true)
                {
                    Console.WriteLine("Saving new file after update...");
                    filesRepo.SaveQuotations(recordsFromWeb);
                    Console.WriteLine("File saved.");
                }
            }
        }
    }
}
