﻿using System;
using System.Collections.Generic;
using System.Net;

namespace SkyriseProject
{
    class WebService
    {
        private CsvMapper _csvMapper = new CsvMapper();
        private FilesRepo _filesRepo = new FilesRepo();
        private WebClient _client = new WebClient();

        public string[] CsvLinks =
        {
                "https://stooq.pl/q/l/?s=wig&f=sd2t2ohlcv&h&e=csv",
                "https://stooq.pl/q/l/?s=wig20&f=sd2t2ohlcv&h&e=csv",
                "https://stooq.pl/q/l/?s=fw20&f=sd2t2ohlcvi&h&e=csv",
                "https://stooq.pl/q/l/?s=wig20usd&f=sd2t2ohlcv&h&e=csv",
                "https://stooq.pl/q/l/?s=mwig40&f=sd2t2ohlcv&h&e=csv",
                "https://stooq.pl/q/l/?s=swig80&f=sd2t2ohlcv&h&e=csv"
        };

        public List<Record> GetDataFromWeb()
        {
            List<Record> records = new List<Record>();

            foreach (var link in CsvLinks)
            {
                Console.WriteLine($"Trying to read uri: {link}");
                try
                {
                    var stream = _client.OpenRead(link);
                    var record = _csvMapper.FromStreamToRecord(stream);
                    records.Add(record);
                }
                catch (WebException e)
                {
                    Console.WriteLine($"Invalid adress or an error occured while downloading data\n{e}");
                }
                Console.WriteLine("Reading complete.");
            }
            _client.Dispose();
            return records;
        }

        public bool IsRecordUpdated(List<Record> recordFromWeb, List<Record> recordsFromFile)
        {
            Console.WriteLine("Checking for updates in quotations...");

            for (int i = 0; i < recordFromWeb.Count; i++)
            {
                if (DateTime.Compare(recordFromWeb[i].DataCzas, recordsFromFile[i].DataCzas) > 0)
                {
                    return true;
                }
            }
            Console.WriteLine("No updates found.");
            return false;
        }
    }
}
