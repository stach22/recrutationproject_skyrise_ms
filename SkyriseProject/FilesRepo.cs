﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SkyriseProject
{
    class FilesRepo
    {
        private JsonMapper _mapper = new JsonMapper();

        private void Save(string filePath, List<Record> content)
        {
            Console.WriteLine("Saving *.json file...");
            var stringToSave = _mapper.FromRecordsToJson(content);
            File.WriteAllText(filePath, stringToSave);
        }

        private List<Record> Read(string filepath)
        {
            Console.WriteLine("Reading file from directory...");
            var read = File.ReadAllText(filepath);
            return _mapper.FromJsonToRecords(read);
        }

        public void SaveQuotations(List<Record> records)
        {
            var filesRepo = new FilesRepo();
            var dateTimeInFilePath = DateTime.Now.ToString("yyyyMMddHHmmss");
            filesRepo.Save($"{ProgramLoop.ChosenFilePath}{dateTimeInFilePath}_GPW_Quotations.json", records);
        }

        private bool IsAnyFileSaved(DirectoryInfo directory)
        {
            Console.WriteLine("Checking for files...");
            var gpwFiles = directory.GetFiles("*GPW_Quotations.json");

            if(!gpwFiles.Any())
            {
                return false;
            }
            return true;
        }

        public List<Record> LoadLatestJsonFile(string filePath)
        {
            var directory = new DirectoryInfo(filePath);

            if(IsAnyFileSaved(directory) == true)
            {
                Console.WriteLine("Loading latest *.json file...");
                var newestFile = directory.GetFiles("*GPW_Quotations.json").OrderByDescending(f => f.LastWriteTime).First().FullName;
                FilesRepo filesRepo = new FilesRepo();
                return filesRepo.Read(newestFile);
            }
            return null;
        }
    }
}
