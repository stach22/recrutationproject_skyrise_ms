﻿namespace SkyriseProject
{
    class Program
    {
        static void Main()
        {
            var programLoop = new ProgramLoop();

            ConsoleReadHelper.GetFilePathFromUser();
            ConsoleReadHelper.GetIntervalFromUser();
            programLoop.RunWithTimeInterval();
        }
    }
}
