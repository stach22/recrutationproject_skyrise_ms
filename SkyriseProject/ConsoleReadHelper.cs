﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace SkyriseProject
{
    class ConsoleReadHelper
    {
        public static void GetFilePathFromUser()
        {
            Console.WriteLine("Provide path where you want to keep saved quotation files\n" +
                @"according to this pattern 'c:\myFolder\'" +
                "\nOr type 'd' if you want choose the [d]efault working directory...");

            ProgramLoop.ChosenFilePath = GetFilePathRegex();
        }

        public static void GetIntervalFromUser()
        {
            Console.WriteLine("Type time interval for refreshing info (in seconds)...");
            var userInput = GetInt();

            ProgramLoop.RefreshIntervalInSeconds = userInput;
        }

        public static int GetInt()
        {
            int number;

            while (!int.TryParse(Console.ReadLine(), out number) || number < 0)
            {
                Console.WriteLine("Not a numeric value or input out of range (0-max) - try again...");
            }
            return number;
        }

        public static string GetFilePathRegex()
        {
            string userInput;
            bool success;

            do
            {
                userInput = Console.ReadLine();

                if (userInput == "d")
                {
                    return AppDomain.CurrentDomain.BaseDirectory;
                }

                var match = Regex.Match(userInput, @"^[A-z]:\\.{0,}\\$");
                success = match.Success && Directory.Exists(userInput);

                if (!success)
                {
                    Console.WriteLine("Wrong path or directory doesn't exist!\nTry Again...");
                }
            } while (!success);

            return userInput;
        }
    }
}
