﻿using System;

namespace SkyriseProject
{
    class Record
    {
        public string Symbol { get; set; }
        public DateTime DataCzas { get; set; }
        public decimal Otwarcie { get; set; }
        public decimal Najwyzszy { get; set; }
        public decimal Najnizszy { get; set; }
        public decimal Zamkniecie { get; set; }
        public decimal Wolumen { get; set; }
    }
}
